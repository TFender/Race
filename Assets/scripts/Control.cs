﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Control : MonoBehaviour {

    public AudioSource m_MovementAudio;  
    public AudioClip m_EngineIdling;            
    public AudioClip m_EngineDriving;
    public GameObject m_DustPrefab;
    private Slider slider;
    private Rigidbody m_Rigidbody;              
    private float m_OriginalPitch = 1f;         
    private ParticleSystem m_particleSystem;
    private BoxCollider m_collider;
    private KeyCode left;
    private KeyCode right;
    private float distToGround;
    private float maxspeed = 60;
    private float maxreverse = 25;
    private float rotationspeed = 200;
    private float m_speed = 0;
    private float m_reverse = 0;
    private int m_playerNumber;
    bool playing = false;
    [HideInInspector]
    public bool forward = false;
    private int multiplier = 25;

    private void OnEnable()
    {
        m_Rigidbody.isKinematic = false;
        m_speed = 0;
        m_MovementAudio.Play();
        if (m_particleSystem !=null)
        m_particleSystem.Stop();
    }

    private void OnDisable()
    {
        resetSpeed();
        m_MovementAudio.Stop();
        m_Rigidbody.isKinematic = true;
        if (m_particleSystem !=null)
        m_particleSystem.Stop();
    }

    private void Start()
    {
      m_MovementAudio.clip = m_EngineIdling;
      m_Rigidbody = GetComponent<Rigidbody>();
      m_collider = GetComponent<BoxCollider>();
      distToGround = m_collider.bounds.center.y;
      m_particleSystem = Instantiate(m_DustPrefab).GetComponent<ParticleSystem>();
      m_particleSystem.Stop();
    }

    public void setSlider(Slider slider)
    {
        this.slider = slider;
    }

    public void setKeys(int playerNumber)
    {
        m_playerNumber = playerNumber;
    }

    private bool IsGrounded() {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, -Vector3.up, out hit, distToGround + 0.2f))
        {
            if (hit.transform != null)
            {
                if (hit.transform.tag == "ground")
                {
                    if (!m_particleSystem.isPlaying)
                    {
                        m_particleSystem.Play();
                    }
                    if (m_speed > 50) m_speed -= multiplier * Time.deltaTime * 1.2f;
                    return true;
                }
            }
            m_particleSystem.Stop();
            
            return true;
        }
        return false;
    }

    void FixedUpdate()
    {
        IsGrounded();
        if (forward)
        {
            if (m_speed < maxspeed && m_reverse == 0) m_speed += multiplier * Time.deltaTime;
            transform.Rotate(Vector3.up * rotationspeed * Input.GetAxis("Player"+m_playerNumber) * Time.deltaTime);
            //transform.Rotate(Vector3.up * rotationspeed * slider.value * Time.deltaTime);
            if (m_speed < 0) m_speed = 0;
            if (m_speed > 0)
            {
                transform.Translate(Vector3.forward * m_speed * Time.deltaTime);
                m_MovementAudio.pitch = m_OriginalPitch + m_speed / 30;
            }
            transform.position = new Vector3(transform.position.x, 0.1f, transform.position.z);
        }
    }

    private void Update()
    {
        m_particleSystem.transform.position = transform.position;
        if (!m_MovementAudio.isPlaying && gameObject.activeSelf)
        {
            m_MovementAudio.Play();
        }
        EngineAudio();
    }

    private void EngineAudio()
    {
        if (m_speed < 0.1f || m_reverse < 0.1f)
        {
            if (m_MovementAudio.clip == m_EngineDriving)
            {
                m_MovementAudio.clip = m_EngineIdling;
                m_MovementAudio.pitch = m_OriginalPitch;
                m_MovementAudio.Play();
            }
        }
        else
        {
            if (m_MovementAudio.clip == m_EngineIdling)
            {
                m_MovementAudio.clip = m_EngineDriving;
                m_MovementAudio.Play();
            }
        }
    }

    public void resetSpeed()
    { 
        m_speed = 0;
    }
}
