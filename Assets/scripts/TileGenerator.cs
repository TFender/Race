﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TileGenerator : MonoBehaviour {

    public static List<Tile> m_Tiles = new List<Tile>();
    public GameObject m_TilePrefab;
    public GameObject m_RockPrefab;
    public GameObject m_Cactus;
    public GameObject m_Cactus2;
    public GameObject m_Rubble;

    public void Start()
    {
        Tile.setIndex(0);
        initTiles();
    }

    public void AddTile()
    {
        m_Tiles.Add(new Tile());
        m_Tiles[m_Tiles.Count-1].m_Tile = Instantiate(m_TilePrefab, new Vector3(0,0,0), Quaternion.identity) as GameObject;
        m_Tiles[m_Tiles.Count - 1].Setup();
        m_Tiles[m_Tiles.Count - 1].CreateRocks(m_RockPrefab);
        m_Tiles[m_Tiles.Count - 1].CreateRubble(m_Rubble);
        m_Tiles[m_Tiles.Count - 1].CreateCactae(m_Cactus,0f);
        m_Tiles[m_Tiles.Count - 1].CreateCactae(m_Cactus2,-3.9f);
        if (m_Tiles.Count > 6) {
            m_Tiles[0].DeleteTile();
            m_Tiles.RemoveRange(0, 0);
        }
    }

    public void initTiles()
    {
        for (int i = 0; i < 4; i++)
        {
            AddTile();
        }
    }

    public void startAgain()
    {
        foreach (Tile tile in m_Tiles)
        {
            tile.DeleteTile();
        }
        m_Tiles.Clear();
        Tile.setIndex(0);
        initTiles();
    }
}
