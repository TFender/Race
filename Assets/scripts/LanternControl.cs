﻿using UnityEngine;

public class LanternControl : MonoBehaviour
{
	private bool lightson;
	private GameObject Sun;

	void Start()
	{
		Sun = GameObject.Find("Sun");
	}

	// Update is called once per frame
	void Update ()
	{
		var x = Sun.transform.eulerAngles.x;
		if (x <= 10f || x >= 100f)
		{
			lightson = true;
		}
		else
		{
			lightson = false;
		}
		GetComponentInChildren<Light>().enabled = lightson;
	}
}
