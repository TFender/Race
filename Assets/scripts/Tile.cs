﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour {

    [HideInInspector]public GameObject m_Tile;
    private int m_Difficulty;
    private static int index;
    private Vector3 newPos;
    private GameObject parent;
    public List<GameObject> rocks;
    public List<GameObject> rubble;
    public List<GameObject> cactae;

    public void Setup()
    {
        parent = new GameObject("Tile" + index);
        if (index < 25) { m_Difficulty = index; }
        else { m_Difficulty = 25; }
        m_Tile.transform.parent = parent.transform;
        newPos = new Vector3(0, 0, 120f * index);
        m_Tile.transform.position = newPos;
        m_Tile.transform.Rotate(0,90,0);
        index++;
    }

    public static void setIndex(int newIndex)
    {
        index = newIndex;
    }

    public GameObject getTile()
    {
        return m_Tile;
    }

    public void CreateRocks(GameObject RockPrefab)
    {
        rocks = new List<GameObject>();
        for (int i = 0; i < m_Difficulty; i++)
        {
            GameObject Rock = Instantiate(RockPrefab, new Vector3(0, 0, 0), Quaternion.identity);
            Rock.transform.parent = parent.transform;
            newPos = new Vector3(-20+getRandomNumber(-5, -55, 5, 55), 1, (index*120f-120f)+Random.Range(-57,57));
            Rock.transform.position = newPos;
            Rock.transform.Rotate(Random.Range(0,360), Random.Range(0, 360), Random.Range(0, 360));
            Rock.transform.localScale = new Vector3(Random.Range(1.5f,3.5f), Random.Range(1.5f, 3.5f), Random.Range(1.5f,3.5f));
            rocks.Add(Rock);
        }
    }

    public void CreateRubble(GameObject PebblePrefab)
    {
        rubble = new List<GameObject>();
        for (int i = 0; i <Random.Range(80,100); i++)
        {
            GameObject Pebble = Instantiate(PebblePrefab, new Vector3(0, 0, 0), Quaternion.identity); 
            Pebble.transform.parent = parent.transform;
            newPos = new Vector3(-20 + getRandomNumber(-50,-120,50,120), 0.2f, (index * 120f - 120f) + Random.Range(-57, 57));
            Pebble.transform.position = newPos;
            Pebble.transform.Rotate(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360));
            Pebble.transform.localScale = new Vector3(Random.Range(.2f, .4f), Random.Range(.2f, .4f), Random.Range(.2f, .4f));
            rubble.Add(Pebble);
        }
    }

    public void CreateCactae(GameObject CactusPrefab, float posY)
    {
        cactae = new List<GameObject>();
        for (int i = 0; i < Random.Range(0, 20); i++)
        {
            GameObject Cactus = Instantiate(CactusPrefab, new Vector3(0, 0, 0), Quaternion.identity);
            Cactus.transform.parent = parent.transform;
            newPos = new Vector3(-20 + getRandomNumber(-70, -120, 70, 120), posY, (index * 120f - 120f) + Random.Range(-57, 57));
            Cactus.transform.position = newPos;
            Cactus.transform.Rotate(-90,0, Random.Range(0, 360));
            Cactus.transform.localScale = new Vector3(Random.Range(0.7f, 1.3f), Random.Range(0.7f, 1.3f), Random.Range(0.7f, 1.3f));
            cactae.Add(Cactus);
        }
    }

    public void DeleteTile()
    {
        Destroy(m_Tile);
        foreach (GameObject rock in rocks){
            Destroy(rock);
        }
        foreach (GameObject pebble in rubble)
        {
            Destroy(pebble);
        }
        foreach (GameObject cactus in cactae)
        {
            Destroy(cactus);
        }
    }

    public float getRandomNumber(int a1, int a2, int b1, int b2)
    {
        if (Random.Range(0,11) > 5)
        {
            return Random.Range(a1, a2);
        }
        return Random.Range(b1, b2);
    }
}
