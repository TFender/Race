﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlinkingHeader : MonoBehaviour
{

	private Text _textComponent;
	private string _text;
	
	void Start ()
	{
		_textComponent = GetComponent<Text>();
		_text = _textComponent.text;
		StartCoroutine(blink());
	}

	private IEnumerator blink()
	{
		while (true)
			
		{
			_textComponent.text = _text;
			yield return new WaitForSeconds(0.8f);
			_textComponent.text = "";
			yield return new WaitForSeconds(0.4f);
		}
	}
}
