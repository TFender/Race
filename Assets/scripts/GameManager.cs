﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Complete
{
    public class GameManager : MonoBehaviour
    {           
        private float m_StartDelay = 2f;             
        private float m_Pause = 1.5f;
        public CameraControl m_CameraControl;      
        public Text m_MessageText;
        public GameObject m_CarPrefab;
        public CarManager[] m_Cars = new CarManager[2];
        public GameObject tilegen;
        private int m_RoundNumber;                 
        private CarManager m_RoundWinner;          
        private CarManager m_GameWinner;
        private bool first = true;

        private void Start() {
            Instantiate(tilegen, new Vector3(), Quaternion.identity);
            GetComponent<AudioSource>().Play();
            SpawnAllCars();
            DisableCarControl();
            SetCameraTargets();
            StartCoroutine(GameLoop());
            m_MessageText.text = Variables.roundsToWin + " Rounds To Win!";
        }

        private void SpawnAllCars()
        {
            for (int i = 0; i < m_Cars.Length; i++)
            {
                m_Cars[i].m_Instance = Instantiate(m_CarPrefab, m_Cars[i].m_SpawnPoint.position, m_Cars[i].m_SpawnPoint.rotation);
                m_Cars[i].m_PlayerNumber = i + 1;
                m_Cars[i].Setup();
            }
        }

        private void SetCameraTargets()
        {
            Transform[] targets = new Transform[m_Cars.Length];

            for (int i = 0; i < targets.Length; i++)
            {
                targets[i] = m_Cars[i].m_Instance.transform;
            }
            m_CameraControl.m_Targets = targets;
        }


        private IEnumerator GameLoop()
        {
            yield return StartCoroutine(RoundStarting());
            yield return StartCoroutine(RoundPlaying());
            yield return StartCoroutine(RoundEnding());

            if (m_GameWinner != null)
            {                
                yield return new WaitForSeconds(2f);
                SceneManager.LoadScene("MainMenu");
            }
            else
            {
                tilegen.GetComponent<TileGenerator>().startAgain();
                StartCoroutine(GameLoop());
            }
        }

        private IEnumerator FadeOutTheme()
        {
            while (GetComponent<AudioSource>().volume > 0)
            {
                GetComponent<AudioSource>().volume -= 0.05f;
                yield return new WaitForSeconds(0.08f);
            }
        }

        private IEnumerator RoundStarting()
        {   
            ResetAllCars();
            DisableCarControl();
            m_RoundNumber++;
            yield return new WaitForSeconds(m_StartDelay);
            m_MessageText.text = "";
            yield return new WaitForSeconds(1.0f);
            m_MessageText.text = "ROUND " + m_RoundNumber;
            m_CameraControl.SetStartPositionAndSize();
            yield return new WaitForSeconds(m_Pause);
            m_MessageText.text = "";
            yield return new WaitForSeconds(1.0f);
        }


        private IEnumerator RoundPlaying()
        {
            EnableCarControl();
            for (int i = 0; i < 5; i++)
            {
                m_MessageText.text = "GO!!!";
                yield return new WaitForSeconds(0.3f);
                m_MessageText.text = "";
                yield return new WaitForSeconds(0.3f);
            }
            m_MessageText.text = string.Empty;
            while (!OneCarLeft())
            {
                yield return null;
            }
        }


        private IEnumerator RoundEnding()
        {
            DisableCarControl();
            m_RoundWinner = null;
            m_RoundWinner = GetRoundWinner();
            if (m_RoundWinner != null)
                m_RoundWinner.m_Wins++;
            m_GameWinner = GetGameWinner();
            yield return blinkingEndText(5);
            if (m_GameWinner!=null)
            {
                StartCoroutine(FadeOutTheme());
                StartCoroutine(blinkingEndText(20));
            }
            yield return new WaitForSeconds(1f);
        }

        private IEnumerator blinkingEndText(int times)
        {
            for (int i = 0; i < times; i++)
            {
                m_MessageText.text = EndMessage();
                yield return new WaitForSeconds(0.5f);
                m_MessageText.text = "";
                yield return new WaitForSeconds(0.3f);
            }
        }

        private bool OneCarLeft()
        {
            int numCarsLeft = 0;
            for (int i = 0; i < m_Cars.Length; i++)
            {
                if (m_Cars[i].m_Instance.activeSelf)
                    numCarsLeft++;
            }
            return numCarsLeft <= 1;
        }

        private CarManager GetRoundWinner()
        {
            for (int i = 0; i < m_Cars.Length; i++)
            {
                if (m_Cars[i].m_Instance.activeSelf)
                    return m_Cars[i];
            }
            return null;
        }

        private CarManager GetGameWinner()
        {
            for (int i = 0; i < m_Cars.Length; i++)
            {
                if (m_Cars[i].m_Wins == Variables.roundsToWin)
                    return m_Cars[i];
            }
            return null;
        }

        private string EndMessage()
        {
            string message = "DRAW!";
            if (m_RoundWinner != null)
                message = m_RoundWinner.m_ColoredPlayerText + " WINS THE ROUND!";
            message += "\n\n\n\n";
            for (int i = 0; i < m_Cars.Length; i++)
            {
                message += m_Cars[i].m_ColoredPlayerText + ": " + m_Cars[i].m_Wins + " WINS\n";
            }
            if (m_GameWinner != null)
                message = m_GameWinner.m_ColoredPlayerText + " WINS THE GAME!";

            return message;
        }

        private void ResetAllCars()
        {
            for (int i = 0; i < m_Cars.Length; i++)
            {
                m_Cars[i].Reset();
            }
        }


        private void EnableCarControl()
        {
            for (int i = 0; i < m_Cars.Length; i++)
            {
                StartCoroutine(waitAWhile());
                m_Cars[i].EnableControl();
            }
        }


        private void DisableCarControl()
        {
            for (int i = 0; i < m_Cars.Length; i++)
            {
                m_Cars[i].DisableControl();
            }
        }

        private IEnumerator waitAWhile()
        {
            yield return new WaitForSeconds(0.5f);
        }
    }
}