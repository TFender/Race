﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CarEffects : MonoBehaviour
{         
    public GameObject m_ExplosionPrefab;                
    private AudioSource m_ExplosionAudio;               
    private ParticleSystem m_ExplosionParticles;                            
    public GameObject TileGenerator;
    private Camera mainCamera;
    public Vector2 heightThresold;


    void Update()
    {
        Vector2 screenPosition = mainCamera.WorldToScreenPoint(transform.position);
        if (screenPosition.y < heightThresold.x && !GetComponent<Rigidbody>().isKinematic)
        {
            OnDeath();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("trigger") && (gameObject.tag == "car1" || gameObject.tag == "car2"))
        {
            GameObject triggerBlock = GameObject.FindWithTag("trigger");
            Destroy(triggerBlock);
            TileGenerator.GetComponent<TileGenerator>().AddTile();
        }
    }

    private void OnCollisionEnter(UnityEngine.Collision collision)
    {
        if (collision.gameObject.tag!="ground"||collision.gameObject.tag != "road"||collision.gameObject.tag!="trigger")
        OnDeath();
    }

    private void Start()
    {
        m_ExplosionParticles = Instantiate(m_ExplosionPrefab).GetComponent<ParticleSystem>();
        m_ExplosionAudio = m_ExplosionParticles.GetComponent<AudioSource>();
        m_ExplosionParticles.gameObject.SetActive(false);
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
    }

    private void OnDeath()
    {
        m_ExplosionParticles.transform.position = transform.position;
        m_ExplosionParticles.gameObject.SetActive(true);
        m_ExplosionParticles.Play();
        m_ExplosionAudio.Play();
        gameObject.SetActive(false);
    }
}
