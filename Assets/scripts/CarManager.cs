﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[System.Serializable]
public class CarManager
    {
        public Color m_PlayerColor;
        public Transform m_SpawnPoint;

        [HideInInspector] public int m_PlayerNumber;
        [HideInInspector] public string m_ColoredPlayerText;
        [HideInInspector] public GameObject m_Instance;
        [HideInInspector] public int m_Wins;

        //public Slider slider;
        private Control m_Movement;        
        public void Setup()
        {
           // slider.enabled = true;
            m_Movement = m_Instance.GetComponent<Control>();
           // slider.minValue = -0.60f;
           // slider.maxValue = 0.60f;
           // slider.value = 0;
         //   m_Movement.setSlider(slider);
            m_Movement.setKeys(m_PlayerNumber);
            m_ColoredPlayerText = "<color=#" + ColorUtility.ToHtmlStringRGB(m_PlayerColor) + ">PLAYER " + m_PlayerNumber + "</color>";
            Renderer[] renderers = m_Instance.GetComponentsInChildren<Renderer>();
            for (int i = 0; i < renderers.Length; i++)
            {
                if (renderers[i].name == "chassis") renderers[i].material.color = m_PlayerColor;
            }
            m_Instance.tag = "car" + m_PlayerNumber;
        }

        public void DisableControl()
        {
            m_Movement.resetSpeed();
            m_Movement.enabled = false;
            m_Movement.forward = false;
        }

        public void EnableControl()
        {
            m_Movement.enabled = true;
            m_Movement.forward = true;
            m_Instance.GetComponent<AudioSource>().Play();
    }

        public void Reset()
        {
            m_Instance.GetComponent<AudioSource>().Stop();
            m_Instance.transform.position = m_SpawnPoint.position;
            m_Instance.transform.rotation = m_SpawnPoint.rotation;
            m_Instance.SetActive(false);
            m_Instance.SetActive(true);
        }
    }

