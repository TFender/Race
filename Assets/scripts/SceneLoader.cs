﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoader : MonoBehaviour
{
    private GameObject m_backGroundImage;
    private SpriteRenderer m_sprite;
    public Text Rounds;
    public Button[] buttons;
    
    void Start()
    {
        m_backGroundImage = GameObject.Find("BackGroundImage");
        m_sprite = m_backGroundImage.GetComponent<SpriteRenderer>();
        GetComponent<AudioSource>().Play();
        Rounds.text = Variables.roundsToWin.ToString();
    }

    public void LoadScene(string scene)
    {
        foreach (var b in buttons)
        {
            b.interactable = false;
        }
        StartCoroutine(transition(scene));
        StartCoroutine(fadeOutTheme());
    }

    public void IncreaseRound()
    {
        if (Variables.roundsToWin != 9) ++Variables.roundsToWin;
        Rounds.text = Variables.roundsToWin.ToString();
    }

    public void DecreaseRound()
    {
        if (Variables.roundsToWin != 1) --Variables.roundsToWin;
        Rounds.text = Variables.roundsToWin.ToString();
    }
    
    private IEnumerator transition(string scene)
    {
        var x = 0;
        while (x < 100) 
        {
            m_backGroundImage.transform.localScale += new Vector3(0.03f,0.03f,0);
            var color = m_sprite.color;
            var c = 0.01f;
            m_sprite.color = new Color(color.r-=c, color.g-=c, color.b-=c);
            yield return new WaitForSeconds(0.01f);
            x++;
        }
        SceneManager.LoadSceneAsync(scene); 
    }
    
    private IEnumerator fadeOutTheme()
    {
        while (GetComponent<AudioSource>().volume > 0)
        {
            GetComponent<AudioSource>().volume -= 0.05f;
            yield return new WaitForSeconds(0.08f);
        }
    }
}
