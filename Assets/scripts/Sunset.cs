﻿using System;
using UnityEngine;

public class Sunset : MonoBehaviour
{
	public float sunset = 80f;
	private float sunrotation = 120f;

	// Use this for initialization
	private void Start()
	{
		transform.eulerAngles = new Vector3(sunset, sunrotation, transform.rotation.z);
	}
	
	// Update is called once per frame
	private void Update()
	{
		sunset %= 360;
		if (sunset >= 180f)
		{
			sunset += 4f * Time.deltaTime;
		}
		sunset += 1.2f * Time.deltaTime;
		transform.eulerAngles = new Vector3(sunset, sunrotation, transform.rotation.z);
		
		GetComponentInChildren<Light>().intensity = getIntensity(sunset);
	}

	private float getIntensity(float sunset)
	{
		return (float)(0.40 *(Math.Sin((sunset * 2 * Math.PI)/360D)) + 0.40);
	}
}
